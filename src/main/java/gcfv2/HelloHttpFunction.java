package gcfv2;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.google.cloud.functions.HttpFunction;
import com.google.cloud.functions.HttpRequest;
import com.google.cloud.functions.HttpResponse;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class HelloHttpFunction implements HttpFunction {
    public void service(final HttpRequest request, final HttpResponse response) throws Exception {
        String[] links = {
                "https://jira.ontrq.com/status",
                "https://jira-stage.ontrq.com/status",
//                "http://jira-test-01.ecofabric.com:8080/",
//                "http://jira-test-02.ecofabric.com:8080/",
//                "http://jira-test-03.ecofabric.com:8080/",
//                "http://jira-test-04.ecofabric.com:8080/",
//                "https://confluence-stage.ontrq.com/status",
                "https://confluence.ontrq.com/status"
        };

        Gson gson = new Gson();
        HttpClient client = HttpClient.newBuilder().build();
        Map<String, String> statuses = new HashMap<>();
        
        Arrays.stream(links).forEach(link -> {
            var get = java.net.http.HttpRequest.newBuilder()
                    .GET()
                    .uri(URI.create(link))
                    .build();
            try {
                var resp = client.send(get, java.net.http.HttpResponse.BodyHandlers.ofString());
                JsonObject status = gson.fromJson(resp.body(), JsonObject.class);
                statuses.put(link, status.getAsJsonObject().get("state").getAsString());
            } catch (IOException | InterruptedException e) {
                throw new RuntimeException(e);
            }
        });

//        var data = gson.toJson(statuses);
        
        response.appendHeader("Content-Type", "application/json");
        response.appendHeader("Country", "Ukraine");
        response.appendHeader("Access-Control-Allow-Origin", "https://ac-monitors.vercel.app");
        
        final BufferedWriter writer = response.getWriter();

        writer.write( 
                String.format(
                    "[{\"%s\":\"%s\"},{\"%s\":\"%s\"},{\"%s\":\"%s\"}]",
                    "jiraProd", statuses.get("https://jira.ontrq.com/status"),
                    "jiraStage", statuses.get("https://jira-stage.ontrq.com/status"),
                    "confProd", statuses.get("https://confluence.ontrq.com/status")
                ));
    }
}
