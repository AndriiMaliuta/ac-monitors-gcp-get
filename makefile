deploy:
    gcloud functions deploy ac-monitors-2 \
            --gen2 \
            --trigger-http \
            --runtime=java17 \
            --entry-point=gcfv2.HelloHttpFunction \
            --allow-unauthenticated \
            --memory=256MB

logs:
    gcloud functions logs read
    gcloud functions logs read FUNCTION_NAME --execution-id EXECUTION_ID